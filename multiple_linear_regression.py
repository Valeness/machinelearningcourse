from __future__ import division
# import numpy as np
# import matplotlib.pyplot as plt
import pandas as pd
# from sklearn.linear_model import LinearRegression
from eve import EVE
import csv


eve = EVE()

# Preprocess Eve Data to get a meaningful matrix
# We want to find out if incursions in a system has any impact on ship prices in that system

new_matrix = []
# with open('data/orderset-4.csv', 'r') as f:
#     reader = csv.DictReader(f)
#     for row in reader:
#         item_id = row['type_id']
#         if(item_id == 34):
#             item = eve.get_item(item_id)
#             print("{0} - {1}".format(item['type_id'], item['name']))
        # location_id = row['location_id']
        # station = eve.get_station(location_id)
        # system = eve.get_system(station['system_id'])
        # print(system)
        # quit()


wars = eve.get_wars()
for i in wars:
    war = eve.get_war(i)
    ships_killed = war['aggressor']['ships_killed'] + war['defender']['ships_killed']
    if(ships_killed > 0):
        killmails = eve.get_war_killmails(i)
        for k in killmails:
            killmail = eve.get_killmail(k['killmail_id'], k['killmail_hash'])
            print(killmail)
            quit()

quit()

orders = eve.get_market_history(10000043, 34)
for i in orders:
    type_id = i['type_id']
    if(type_id == 34):
        item = eve.get_item(type_id)
        print(i)
        quit()

quit()
dataset = pd.read_csv('data/orderset-1.csv')
# dataset = pd.read_csv('data/50_Startups.csv')

x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values

print(y)
quit()

# Encode Categorical Data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_x = LabelEncoder()
x[:, 3] = labelencoder_x.fit_transform(x[:,3])
onehotencoder = OneHotEncoder(categorical_features=[3])
x = onehotencoder.fit_transform(x).toarray()

# Avoiding the Dummy Variable Trap
x = x[:, 1:]

# Split the data into a test and training set
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = .2, random_state = 0)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(x_train, y_train)

# Predicting the Test Set Results
y_pred = regressor.predict(x_test)