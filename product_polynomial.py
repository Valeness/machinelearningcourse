from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

dataset = pd.read_csv('data/discount_percent.csv')

x = dataset.iloc[:, 1:3].values
y = dataset.iloc[:, 3:4].values

# Encode Categorical Data
# from sklearn.preprocessing import LabelEncoder, OneHotEncoder
# labelencoder_x = LabelEncoder()
# x[:, 3] = labelencoder_x.fit_transform(x[:,3])
# onehotencoder = OneHotEncoder(categorical_features=[3])
# x = onehotencoder.fit_transform(x).toarray()

# Avoiding the Dummy Variable Trap
# x = x[:, 1:]

# Split the data into a test and training set
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = .2, random_state = 0)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(x_train, y_train)

# print(x_test)

# Predicting the Test Set Results
# Need to calculate p-value to see relevance of discount_percent and homepage_position
y_pred = regressor.predict([
    [0, 85],
    [0, 35],
    [0, 45],
    [0, 55],
    [0, 65],
    [0, 75],
    [1, 15],
    [1, 35],
    [1, 45],
    [1, 55],
    [1, 65],
    [1, 75],
    [1, 85],
    [2, 15],
    [2, 35],
    [2, 45],
    [2, 55],
    [2, 65],
    [2, 75],
    [2, 85],
    [3, 15],
    [3, 35],
    [3, 45],
    [3, 55],
    [3, 65],
    [3, 75],
    [3, 85],
])
print(y_pred)
