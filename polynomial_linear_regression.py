from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

dataset = pd.read_csv('data/ferrogel_history.csv')

x = dataset.iloc[:, 0:1].values
y = dataset.iloc[:, 4:5].values

# Split the data into a test and training set
# from sklearn.model_selection import train_test_split
# x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = .2, random_state = 0)

# Fit Linear Model
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(x, y)

# Fit Polynomial Regression
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 15)
x_poly = poly_reg.fit_transform(x)

lin_reg2 = LinearRegression()
lin_reg2.fit(x_poly, y)

# plt.scatter(x, y, color = "red")
plt.plot(x, lin_reg.predict(x), color="green")
# plt.title('Truth or Bluff (Linear Regression)')
# plt.xlabel('Position Level')
# plt.ylabel('Salary')
# plt.show()

x_grid = np.arange(min(x), max(x) + 3, 0.1)
x_grid = x_grid.reshape((len(x_grid), 1))
plt.scatter(x, y, color = "red")
plt.plot(x_grid, lin_reg2.predict(poly_reg.fit_transform(x_grid)), color="blue")
plt.title('Ferrogel Price over Time (Polynomial Regression)')
plt.xlabel('Days since October 1st, 2016')
plt.ylabel('Price')
plt.show()

# Predicting a new result with Linear Regression
# print(lin_reg.predict(6.5))

# Predicting a new result with Polynomial Regression
print(lin_reg2.predict(poly_reg.fit_transform(431)))