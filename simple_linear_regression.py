from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from eve import EVE
import time, datetime, csv

# Turn api data into a csv that this function can read
eve = EVE()
orders = eve.get_market_history(10000043, 16683)
new_csv = []
counter = 0
with open('data/ferrogel_history.csv', 'wb') as f:
    wr = csv.writer(f, delimiter=',')
    wr.writerow(['date', 'volume', 'lowest', 'highest', 'average'])
    for i in orders:
        string_date = i['date']
        date = time.mktime(datetime.datetime.strptime(string_date, '%Y-%m-%d').timetuple())
        # print(i)
        # quit()
        wr.writerow([counter, i['volume'], i['lowest'], i['highest'], i['average']])
        counter+=1
        # new_csv.append({'date' : date, 'volume' : i['volume'], 'lowest' : i['lowest'], 'highest' : i['highest'], 'average' : i['average']})
        # quit()

quit()
dataset = pd.read_csv('data/adaptive_history.csv')
x = dataset.iloc[:, 0:1].values
y = dataset.iloc[:, 4:5].values

from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = .3, random_state = 0)

regressor = LinearRegression()
regressor.fit(x_train, y_train)

y_pred = regressor.predict(x_test)

plt.scatter(x_test, y_test, color='red')
plt.plot(x_train, regressor.predict(x_train), color='blue')
plt.title('Salary vs Experience (Training Set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()
