import requests, redis, hashlib, json

class EVE:
    def __init__(self):
        self.url = 'https://esi.tech.ccp.is/latest'
        # self.redis = redis.StrictRedis(host='54.218.60.183', port=6379)
        self.redis = redis.StrictRedis(host='localhost', port=6379)
        pass


    def get(self, endpoint):
        cache_key = hashlib.md5(endpoint).digest()
        ret = {}
        if(self.redis.exists(cache_key)):
            ret = json.loads(self.redis.get(cache_key))
        else:
            ret = requests.get(self.url + endpoint).json()
            self.redis.set(cache_key, json.dumps(ret))
            self.redis.expire(cache_key, 60 * 60)
        return ret

    def get_system(self, system_id):
        return self.get('/universe/systems/{0}'.format(system_id))

    def get_station(self, station_id):
        return self.get('/universe/stations/{0}'.format(station_id))

    def get_item(self, type_id):
        return self.get('/universe/types/{0}'.format(type_id))

    def get_market_orders(self, region_id):
        return self.get('/markets/{0}/orders'.format(region_id))

    def get_market_history(self, region_id, type_id):
        return self.get('/markets/{0}/history?type_id={1}'.format(region_id, type_id))

    def get_wars(self):
        return self.get('/wars')

    def get_war(self, war_id):
        return self.get('/wars/{0}'.format(war_id))

    def get_war_killmails(self, war_id):
        return self.get('/wars/{0}/killmails'.format(war_id))

    def get_killmail(self, killmail_id, killmail_hash):
        return self.get('/killmails/{0}/{1}'.format(killmail_id, killmail_hash))